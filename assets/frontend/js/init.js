(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
	//$('.slider').slider();
	
	$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
	});
  }); // end of document ready
})(jQuery); // end of jQuery name space
